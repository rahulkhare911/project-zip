
import "./TableInsertComponent.css"

import React, { useState } from 'react';

function ValueInsertComponent() {
  const [values, setValues] = useState([]);
  const [selectedOption, setSelectedOption] = useState('');
  const [inputValue, setInputValue] = useState('');

  const handleAddValue = () => {
    if (selectedOption && inputValue) {
      // Check if the option already exists
      const existingOption = values.find((value) => value.option === selectedOption);

      if (existingOption) {
        // If the option exists, add the value to it
        existingOption.values.push(inputValue);
      } else {
        // If the option doesn't exist, create a new option
        const newValue = { option: selectedOption, values: [inputValue] };
        setValues([...values, newValue]);
      }

      // Clear input fields
      setSelectedOption('');
      setInputValue('');
    }
  };

  const handleDeleteValue = (option, index) => {
    console.log(option, index)
    const updatedValues = values.map((value) => {
      if (value.option === option) {
        value.values.splice(index, 1);
      }
      return value;
    });

    // Remove options with empty values
    const filteredValues = updatedValues.filter((value) => value.values.length > 0);

    setValues(filteredValues);
  };

  return (
    <div className="value-insert-component">
      <div>
        <select
          value={selectedOption}
          onChange={(e) => setSelectedOption(e.target.value)}
        >
          <option value="">Select Option</option>
          <option value="id">ID</option>
          <option value="source_ip">Source IP</option>
          <option value="count">Count</option>
        </select>
        <input
          type="text"
          placeholder="Enter Value"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
        />
        <button onClick={handleAddValue}>Add</button>
      </div>

      <div className="value-chips">
        {values.map((value) => (
          <div key={value.option} className="value-chip">
            {value.option}:
            { value.values.map((val, index) => (

              <span key={index} className="chip">
                {val+","}
              </span>
              
            ))}
            
                <span
                  className="delete-button"
                  onClick={() => handleDeleteValue(value.option, value.values.length -1 )}
                >
                  X
                </span>
            
          
            
          </div>
        ))}
      </div>
    </div>
  );
}

export default ValueInsertComponent;


